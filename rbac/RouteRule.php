<?php
/**
 * @author 595949289@qq.com
 * @date 2024/12/29 9:25
 */

namespace CuiFox\yii\rbac;

use yii\rbac\Rule;

class RouteRule extends Rule
{
    const RULE_NAME = 'routeRule';

    /**
     * @var string
     */
    public $name = self::RULE_NAME;

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        $routeParams = isset($item->data['params']) ? $item->data['params'] : [];
        foreach ($routeParams as $key => $value) {
            if (!array_key_exists($key, $params) || $params[$key] != $value) {
                return false;
            }
        }
        return true;
    }
}