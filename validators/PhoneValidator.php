<?php

namespace CuiFox\yii\validators;

use yii\validators\Validator;

class PhoneValidator extends Validator
{
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $result = preg_match('/^1[0-9]{10}$/',
                $model->$attribute) && strlen($model->$attribute) == 11;
        if (!$result) {
            $this->addError($model, $attribute, '手机格式无效');
        }
    }
}