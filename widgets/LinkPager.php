<?php

namespace CuiFox\yii\widgets;

class LinkPager extends \yii\widgets\LinkPager
{
    public function show()
    {
        return $this->renderPageButtons();
    }
}